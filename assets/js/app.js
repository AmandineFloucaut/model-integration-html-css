console.log('app.js chargé');

const sliderElement = document.querySelector(".header__slider");

let indexImg = 0;
let indexImgP = 6;

function init(){
    createButton();

    const buttonNext = sliderElement.querySelector(".button-next");
    buttonNext.addEventListener('click', handleChangeImageWithButtonNext);

    const buttonPrevious = sliderElement.querySelector(".button-previous");
    buttonPrevious.addEventListener('click', handleChangeImageWithButtonPrevious);

    // TODO
    const pucesElement = document.querySelectorAll(".slider__puces");
    pucesElement.forEach(puce => puce.addEventListener('click', handleChangeImageWithPuces));
}

/**
 * 
 * @param {PointerEvent} event 
 */
function handleChangeImageWithButtonNext(event){
    indexImg++;

    const imagesSlide = document.querySelectorAll(".header__image");

    imagesSlide.forEach((image) =>{
        image.classList.remove("image--is-on");
    });

    if(indexImg === 6){
        indexImg = 0;
    }
    imagesSlide[indexImg].classList.add("image--is-on");
}

/**
 * 
 * @param {PointerEvent} event 
 */
 function handleChangeImageWithButtonPrevious(event){
    indexImgP--;

    const imagesSlide = document.querySelectorAll(".header__image");
    console.log(imagesSlide);
    console.log(imagesSlide[indexImgP]);
    imagesSlide.forEach(image => image.classList.remove("image--is-on"));

    if(indexImgP === 0){
        indexImgP = 6;
    }

    console.log(imagesSlide[indexImgP]);
    imagesSlide[indexImgP].classList.add("image--is-on");
    // indexImg--;
}

/**
 * 
 * @param {PointerEvent} event 
 */
 function handleChangeImageWithPuces(event){
    indexImg++;


    const imagesSlide = document.querySelectorAll(".header__image");

    imagesSlide.forEach((image, index) =>{
        image.getAttribute(indexImg);
        image.classList.remove("image--is-on");
    });

    if(indexImg === 6){
        indexImg = 0;
    }
    imagesSlide[indexImg].classList.add("image--is-on");
}


//===================== BUTTONS MANAGEMENT ========================//

function createButton(){
    // Button next and previous
    injectDivElement(["slider__button", "button-previous"], "<");
    injectDivElement(["slider__button", "button-next"], ">");
    injectPucesElement();
}

/**
 * Method for add new div element in slider div exixting
 * @param {*} arrayClasses (class name to add in attributes)
 * @param {*} content (content to display in div)
 */
function injectDivElement(arrayClasses, content = ""){
    // Create div element with class for inject chevron next and previous in
    const slider__buttonDivElement = createElementWithClasses('div', arrayClasses);

    // Add the new div element in sliderElement + add content if needed
    sliderElement.appendChild(slider__buttonDivElement);
    slider__buttonDivElement.textContent = content;
}

/**
 * Method for inject 6 div puces in div puces container and inject it last in slider element
 */
function injectPucesElement(){
    const divPucesElement = createElementWithClasses('div', ["slider__puces"]);
    sliderElement.appendChild(divPucesElement);

    const pucesGlobalElement = document.querySelector(".slider__puces");

    for(let i = 0 ; i < 6 ; i ++){
        // Create new div puce directly in appendChild, because if is define before, javascript think i inject the same element (the same reference in DOM)
        pucesGlobalElement.appendChild(createElementWithClasses('div', ["puce-image"]));
    }
}

/**
 * Method for create element (for button next, previous and puces) with class element in dom
 * @param {HTMLElementTagNameMap, classesName}
 * @return html element
 */
function createElementWithClasses(element, arrayClasses){
    const slider__buttonElement =  document.createElement(element);

    // NOTE "..." éclate le tableau et ajoute chaque élément comme un paramètre de la fonction 
    slider__buttonElement.classList.add(...arrayClasses);

    return slider__buttonElement;
}


// Listen all events on the DOM
document.addEventListener("DOMContentLoaded", init);
